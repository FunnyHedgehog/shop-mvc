package rc.bootsecurity.db;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DbInit implements CommandLineRunner {
    private static final String IMAGE_URL = "https://lavazza.kiev.ua/444/lavazza-tierra-colombia-arabica-100.jpg";

    private final ProductRepository productRepository;

    public DbInit(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) {
        List<Product> products = IntStream.range(1, 4)
                .mapToObj(i -> new Product(null, "Lavazza coffee 1000g",
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                        IMAGE_URL,
                        99.99))
                .collect(Collectors.toList());
        productRepository.saveAll(products);
    }
}
