package rc.bootsecurity.service;

import org.springframework.stereotype.Service;
import rc.bootsecurity.model.Cart;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.repository.CartRepository;
import rc.bootsecurity.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartService {
    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    public CartService(CartRepository cartRepository, ProductRepository productRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
    }

    public Cart getCartById(Integer userId) {
        Cart cart = cartRepository.findCartById(userId);
        if (cart == null) {
            cart = createDefaultCart(userId);
        }
        return cart;
    }

    public Cart addProductToCart(Integer userId, Integer productId) {
        Cart cart = cartRepository.findCartById(userId);
        if (cart == null) {
            cart = createDefaultCart(userId);
        }
        cart.getProducts().add(productId);
        return cartRepository.save(cart);
    }

    private Cart createDefaultCart(Integer id) {
        Cart defaultCart = new Cart();
        defaultCart.setId(id);
        return cartRepository.save(defaultCart);
    }

    public List<Product> getProductsInTheCart(Cart cart) {
        return cart.getProducts()
                .stream()
                .map(productRepository::findProductById)
                .collect(Collectors.toList());
    }

}
