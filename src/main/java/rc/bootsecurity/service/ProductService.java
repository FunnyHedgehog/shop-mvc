package rc.bootsecurity.service;

import org.springframework.stereotype.Service;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getProductById(Integer id) {
        return productRepository.findProductById(id);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }
}
