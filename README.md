## Overview
The following repo contains example of MVC using Spring Boot and Thymleaf

## Guidelines
To run this project locally you must have installed JDK 11 and Maven 3.

# Build
```shell
mvn clean install
```
# Run
```shell
mvn spring-boot:run
```
After running visit `localhost:8443/`

#Database
In the project used embedded database H2.
All products and users are added to DB in the code on application startup.
